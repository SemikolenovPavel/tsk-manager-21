package ru.t1.semikolenov.tm.command.system;

import ru.t1.semikolenov.tm.api.model.ICommand;
import ru.t1.semikolenov.tm.command.AbstractCommand;

import java.util.Collection;

public final class HelpCommand extends AbstractSystemCommand{

    public static final String NAME = "help";

    public static final String DESCRIPTION = "Show terminal commands.";

    public static final String ARGUMENT = "-h";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public void execute() {
        System.out.println("[HELP]");
        final Collection<AbstractCommand> commands = getCommandService().getTerminalCommands();
        for (final ICommand command : commands) System.out.println(command);
    }

}
