package ru.t1.semikolenov.tm.api.service;

import ru.t1.semikolenov.tm.api.model.ICommand;
import ru.t1.semikolenov.tm.command.AbstractCommand;

import java.util.Collection;

public interface ICommandService {

    Collection<AbstractCommand> getTerminalCommands();

    void add(AbstractCommand command);

    AbstractCommand getCommandByName(String name);

    AbstractCommand getCommandByArgument(String argument);

}
